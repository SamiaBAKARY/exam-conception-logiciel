fastapi==0.75.0
requests==2.25.1
uvicorn==0.17.5
virtualenv==20.4.7
fastapi-health==0.4.0
python-dotenv==0.20.0
