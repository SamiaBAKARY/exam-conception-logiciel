import json, requests, random
from dotenv import load_dotenv
import os

os.environ
load_dotenv()


with open("rudy.json", "r") as read_file: 
    data = json.load(read_file)


def playlist_karaoke ():
    artistes = list(map(lambda x:x["artiste"],data))
    poids = list(map(lambda x:x["note"],data))
    playlist = []
    while len(playlist)<20: 
        artiste = random.choices(artistes, weights=poids)[0]
        chanson = requests.get (os.environ.get("url") + "/random/" + artiste).json()
        if chanson["lyrics"] != None and chanson not in playlist:
            playlist.append(chanson)
    return playlist 

print(playlist_karaoke())