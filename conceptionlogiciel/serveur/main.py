from fastapi import FastAPI,status
import requests
import random
from dotenv import load_dotenv
import os



os.environ
load_dotenv()

app = FastAPI()


def getidartist(requeteasjson):
    try : 
        table = requeteasjson
        id = table["artists"][0]["idArtist"]
        return {"idartist" : id}
    except : 
        print ("Mets le nom d'un artiste !!")

def getrandomalbum(requeteasjson):
    table2 = requeteasjson
    x = random.randint(0,len(table2))
    album = table2["album"][x]["idAlbum"]
    return {"album" : album}

def getrandomtitle(requeteasjson):
    table3 = requeteasjson
    y = random.randint(0,len(table3))
    track = table3["track"][y]["strTrack"]
    url = table3["track"][y]["strMusicVid"]
    return {"track" : track, "url" : url}

def getlyrics(requeteasjson):
    table4 = requeteasjson
    if "lyrics" in table4 :
        lyrics =  table4["lyrics"] 
    else:
        lyrics = None
    return {"lyrics": lyrics}


def check(lien1,lien2) :
    if requests.get(os.environ.get(lien1)).status_code == 200 and requests.get(os.environ.get(lien2)).status_code ==200 : 
        return 200
    else: 
        return 404

@app.get("/")
def healthcheck(): 
    health = check("lien_artist","lien_lyrics")
    return health
    

@app.get("/random/{artist_name}")
def read_root(artist_name):   
    idArtist = getidartist(requests.get(os.environ.get("api_id").format(artist_name)).json())["idartist"]
    albumArtist = getrandomalbum(requests.get(os.environ.get("api_album").format(idArtist)).json())["album"]
    trackArtist = getrandomtitle(requests.get(os.environ.get("api_track").format(albumArtist)).json())["track"]
    urlsong = getrandomtitle(requests.get(os.environ.get("api_song").format(albumArtist)).json())["url"]
    lyricssong = getlyrics(requests.get(os.environ.get("api_lyrics").format(artist_name,trackArtist.replace("/",""))).json())["lyrics"]
    return {"artist":artist_name, "suggested_youtube_url" :urlsong , "lyrics" : lyricssong, "albumArtist":albumArtist,"trackArtist":trackArtist,"lyricssong": lyricssong}
   

