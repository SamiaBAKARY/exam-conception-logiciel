# Pour exécuter ce projet vous devez impérativement vous mettre à la racine du projet et écrire dans le terminal 
# python3.8 -m pytest conceptionlogiciel/test/test_serveur.py

from conceptionlogiciel.serveur.main import getidartist,getrandomtitle
import pytest,json,random

def load_params_from_json(json_path):
    with open(json_path) as f : 
        return json.load(f)

def test_id_artist():
    data = load_params_from_json("conceptionlogiciel/test/id_artist.json")
    assert getidartist(data) == {"idartist" :"114364"}

def test_id_track():
    random.seed(1)
    data = load_params_from_json("conceptionlogiciel/test/track.json")
    assert getrandomtitle(data) == {"track" : '1+1', "url" : "https://www.youtube.com/watch?v=KaasJ44O5lI"}