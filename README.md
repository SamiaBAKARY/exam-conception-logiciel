
# !!! Bienvenue sur mon application !!!!
Notre application est une API musicale qui: 
- permet de proposer de manière aléatoire une chanson pour un artiste donnée en entrée 
- Contrôle l'état de santé des api cible
- permet d'obtenir une playlist sur la base des d'un fichier json d'artistes favoris

## 1- Installation et lancement de l'application
Pour lancer l'application taper dans votre terminal : 
```
git clone https://gitlab.com/SamiaBAKARY/exam-conception-logiciel.git
cd exam-conception-logiciel
cd conceptionlogiciel
pip install -r requirements.txt
pip install -r client/requirements.txt
pip install -r serveur/requirements.txt
cd serveur
python -m uvicorn main:app --reload
```
Une fois l'application lancée : 
- vous pouvez dans votre moteur de recherche afficher une chanson aléatoire de l'artiste de votre choix 
``` 
http://localhost:8000/random/{nomdelartiste} 
``` 
pour obtenir une chanson aléatoire d'un artiste souhaité

``` 
http://localhost:8000/
 ``` 
pour vérifier l'état des api auxquelles le serveur fait appel (la page doit afficher 200)

- ou obtenir une playlist pour Rudy basée sur le fichier rudy.json en ouvrant un second terminal :
```
cd exam-conception-logiciel
cd conceptionlogiciel
cd client 
python main.py
```
## 2- Test
Certaines fonctions de notre serveur ont été testé. Pour voir le résultat des tests se mettre faite:

```
cd exam-conception-logiciel
python3.8 -m pytest conceptionlogiciel/test/test_serveur.py 
```
!!!! attention !!!! 
Pour lancer le code des tests il faut préciser la version de python ainsi sur la vm c'est : 
```
cd exam-conception-logiciel
python3.8 -m pytest conceptionlogiciel/test/test_serveur.py 
```
et sur le datalab c'est : 
```
cd exam-conception-logiciel
python3.10 -m pytest conceptionlogiciel/test/test_serveur.py 
```

## 3- Diagramme 
```mermaid

graph TD;

  Client -- requête HTTP --> Serveur;

  Serveur -- Résultat de la réquête  --> Client;

  Serveur --  requête HTTP --> AudioDB;

  Serveur --  requête HTTP --> Lyricsovh;

  AudioDB --  json --> Serveur ;

  Lyricsovh -- json --> Serveur ;

```